const c = document.getElementById("tree-canvas") as HTMLCanvasElement;
const nodeColorSelect = document.getElementById(
  "node-color"
) as HTMLSelectElement;
const edgeColorSelect = document.getElementById(
  "edge-color"
) as HTMLSelectElement;
const addToTreeBtn = document.getElementById("add") as HTMLButtonElement;
const undoBtn = document.getElementById("undo") as HTMLButtonElement;
const traverseBtn = document.getElementById("traverse") as HTMLButtonElement;
const downloadBtn = document.getElementById("download") as HTMLButtonElement;
const clearTreeBtn = document.getElementById("clear") as HTMLButtonElement;
const loadSampleTreeBtn = document.getElementById(
  "sample__btn"
) as HTMLButtonElement;
let nodeColorValue = nodeColorSelect?.value;
let edgeColorValue = edgeColorSelect.value;
let nodeRadius = 24;

window.addEventListener("load", () => {
  console.log("Loaded");
});

nodeColorSelect.addEventListener("change", (e) => {
  nodeColorValue = (e.target as HTMLSelectElement).value;
});

edgeColorSelect.addEventListener("change", (e) => {
  edgeColorValue = (e.target as HTMLSelectElement).value;
});

type ActionsStack = Array<{
  node: TreeNode | null;
  action: string;
}>;

// Utility functions
const addAndDisplayNode = (
  x: number,
  y: number,
  r: number,
  ctx: CanvasRenderingContext2D,
  value: number
) => {
  let node = new TreeNode(x, y, r, ctx, value);
  node.draw();
  return node;
};

const removeAndClearNode = (node: TreeNode) => {
  node.clear();
  return null;
};

class Tree {
  root: TreeNode | null;
  ctx: CanvasRenderingContext2D;
  actionsStack: ActionsStack;

  constructor(actionsStack: ActionsStack) {
    this.root = null;
    this.ctx = c.getContext("2d") as CanvasRenderingContext2D;
    this.actionsStack = actionsStack; // stack for storing the actions
  }

  addNode = (value: number) => {
    if (this.root === null) {
      this.root = addAndDisplayNode(400, 30, 24, this.ctx, value);
      this.actionsStack.push({ node: this.root, action: "add" });
      return;
    }

    this.recursiveAddNode(this.root, value);
  };

  recursiveAddNode(currentNode: TreeNode, value: number) {
    if (currentNode.data > value) {
      if (currentNode.left === null) {
        const { cx, cy } = currentNode.leftCoordinate;
        currentNode.left = addAndDisplayNode(cx, cy, 24, this.ctx, value);
        currentNode.drawEdgePath(cx, cy, "l");
        this.actionsStack.push({ node: currentNode.left, action: "add" });
        return;
      }
      this.recursiveAddNode(currentNode.left, value);
    } else {
      if (currentNode.right === null) {
        const { cx, cy } = currentNode.rightCoordinate;
        currentNode.right = addAndDisplayNode(cx, cy, 24, this.ctx, value);
        currentNode.drawEdgePath(cx, cy, "r");
        this.actionsStack.push({ node: currentNode.right, action: "add" });
        return;
      }
      this.recursiveAddNode(currentNode.right, value);
    }
  }

  traverse(store: Array<number>) {
    if (this.root === null) {
      alert("Tree is empty!");
      return;
    }
    this.root.visit(store);
  }

  search(value: number) {
    let found = this.root?.search(value);

    if (found) {
      console.log("Found: ", value);
    } else {
      console.log("Not Found");
    }
  }

  searchNode(node: TreeNode) {
    let parent: TreeNode | null = null;
    let currentNode = this.root;

    while (currentNode !== node && currentNode !== null) {
      parent = currentNode;

      if (currentNode.data > node.data) {
        currentNode = currentNode.left;
      } else {
        currentNode = currentNode.right;
      }
    }

    return parent;
  }

  // Delete a leaf node: to be extended
  deleteNode(node: TreeNode) {
    const parent = this.searchNode(node);
    if (parent === null) {
      if (this.root !== null) {
        this.root = removeAndClearNode(this.root);
        return;
      }
      alert("Tree is empty!");
      return;
    }

    if (parent.left?.data === node.data) {
      const { cx, cy } = parent.leftCoordinate;
      parent.left = removeAndClearNode(node);
      parent.clearEdgePath(cx, cy, "l");
      return;
    }
    if (parent.right?.data === node.data) {
      const { cx, cy } = parent.rightCoordinate;
      parent.right = removeAndClearNode(node);
      parent.clearEdgePath(cx, cy, "r");
      return;
    }
  }

  download(filename: string) {
    /// create an "off-screen" anchor tag
    var lnk = document.createElement("a");
    /// the key here is to set the download attribute of the a tag
    lnk.download = filename;
    /// convert canvas content to data-uri for link. When download
    /// attribute is set the content pointed to by link will be
    /// pushed as "download" in HTML5 capable browsers
    lnk.href = this.ctx.canvas.toDataURL("image/png;base64");
    lnk.click();
  }

  clear() {
    while (this.actionsStack.length) {
      const action = this.actionsStack.pop();
      const node = action?.node as TreeNode;
      tree.deleteNode(node);
    }
  }
}

class TreeNode {
  left: TreeNode | null;
  right: TreeNode | null;
  data: number;
  ctx: CanvasRenderingContext2D;
  x: number;
  y: number;
  r: number;

  constructor(
    x: number,
    y: number,
    r: number,
    ctx: CanvasRenderingContext2D,
    data: number
  ) {
    this.left = null;
    this.right = null;
    this.data = data;
    this.ctx = ctx as CanvasRenderingContext2D;
    this.x = x;
    this.y = y;
    this.r = r;
  }
  // computes the left co-ordinate for the current node
  get leftCoordinate() {
    return { cx: this.x - 3 * this.r, cy: this.y + 3 * this.r };
  }

  // computes the right co-ordinate for the current node
  get rightCoordinate() {
    return { cx: this.x + 3 * this.r, cy: this.y + 3 * this.r };
  }

  draw = () => {
    this.ctx.beginPath();
    this.ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI);
    this.ctx.fillStyle = nodeColorValue;
    this.ctx.fill();

    this.ctx.font = "16pt Calibri";
    this.ctx.fillStyle = "white";
    this.ctx.textAlign = "center";
    this.ctx.fillText(String(this.data), this.x, this.y + 5);
  };

  // for deleting the node from the canvas
  clear = () => {
    this.ctx.beginPath();
    this.ctx.clearRect(
      this.x - this.r - 1,
      this.y - this.r - 1,
      this.r * 2 + 2,
      this.r * 2 + 2
    );
    this.ctx.closePath();
  };

  drawEdgePath = (toX: number, toY: number, direction: "r" | "l") => {
    const moveToX = direction === "r" ? this.x + 20 : this.x - 20;
    const moveToY = this.y + this.r - 10;
    const lineToX = direction === "r" ? toX - 7 : toX + 7;
    const lineToY = toY - this.r + 3;
    this.ctx.beginPath();
    // Begin a new sub-path for drawing the edge
    this.ctx.moveTo(moveToX, moveToY);
    // Move from (moveToX,moveToY) to (lineToX,lineToY)
    this.ctx.lineTo(lineToX, lineToY);
    this.ctx.lineWidth = 1.5;
    this.ctx.strokeStyle = edgeColorValue;
    this.ctx.stroke();
    this.ctx.closePath();
  };

  clearEdgePath = (toX: number, toY: number, direction: "r" | "l") => {
    const moveToX = direction === "r" ? this.x + 20 : this.x - 20;
    const moveToY = this.y + this.r - 10;
    const lineToX = direction === "r" ? toX - 7 : toX + 7;
    const lineToY = toY - this.r + 3;
    this.ctx.beginPath();
    // Begin a new sub-path for drawing the edge
    this.ctx.moveTo(moveToX, moveToY);
    // Move from (moveToX,moveToY) to (lineToX,lineToY)
    this.ctx.lineTo(lineToX, lineToY);
    this.ctx.lineWidth = 3;
    this.ctx.strokeStyle = "#FFFFFF";
    this.ctx.stroke();
    this.ctx.closePath();
  };

  visit(store: Array<number>) {
    if (this.left !== null) {
      this.left.visit(store);
    }
    store.push(this.data);
    if (this.right !== null) {
      this.right.visit(store);
    }
  }

  search(value: number): boolean {
    if (this.data === value) {
      // console.log(this.data,value)
      return true;
    }

    if (value < this.data && this.left !== null) {
      return this.left.search(value);
    } else if (value > this.data && this.right !== null) {
      return this.right.search(value);
    }

    return false;
  }
}

let actionsStack: ActionsStack = [];
let tree = new Tree(actionsStack);

const addToTree = () => {
  console.log("Adding new node to tree...");
  const input = document.getElementById("node-input") as HTMLInputElement;
  const inputArr = input.value.split(",");
  inputArr.map((value) => {
    let nodeValue = parseInt(value);
    if (value) {
      tree.addNode(nodeValue);
      console.log("Node added, Tree: ", tree);
    } else {
      throw new Error("Wrong input");
    }
  });
};

// Traverse the tree
const traverse = () => {
  let store: Array<number> = [];
  tree.traverse(store);
  alert("Traversal result: " + JSON.stringify(store));
};

// Undo
const undo = () => {
  let action = actionsStack.pop();
  if (action) {
    const node = action.node as TreeNode;
    tree.deleteNode(node);
  }
};

addToTreeBtn.addEventListener("click", () => {
  addToTree();
});

undoBtn.addEventListener("click", () => {
  undo();
});

traverseBtn.addEventListener("click", () => {
  traverse();
});

downloadBtn.addEventListener("click", () => {
  tree.download("tree.png");
});

clearTreeBtn.addEventListener("click", () => {
  tree.clear();
});

loadSampleTreeBtn.addEventListener("click", () => {
  tree.clear();
  const sampleTrees = [
    [5, 2, 1, 4, 6, 7, 8, 10, 9, 3],
    [13, 3, 4, 12, 14, 10, 5, 1, 8, 2, 7, 9, 11, 6, 18],
  ];
  const sampleValues =
    sampleTrees[Math.floor(Math.random() * sampleTrees.length)];
  sampleValues.map((value) => {
    tree.addNode(value);
  });
});
