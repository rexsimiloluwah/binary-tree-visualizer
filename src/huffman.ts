const symbolProbForm = document.querySelector(
  ".symbol_prob_form"
) as HTMLFormElement;
const symbolProbFormContainer = document.querySelector(
  ".symbol_prob_form_container"
) as HTMLDivElement;
const symbolProbInputContainerTemplate = document.querySelector(
  "[data-input-container-template]"
) as HTMLTemplateElement;
const addToSymbolProbFormButton = document.querySelector(
  ".add_symbol_container"
) as HTMLButtonElement;
const resultContainer = document.querySelector(".result") as HTMLDivElement;
const resultTable = document.querySelector(".result table") as HTMLTableElement;
const resultStatsContainer = document.querySelector(
  ".stats__container"
) as HTMLTableElement;
const huffmanTreeCanvas = document.getElementById(
  "huffman-tree__canvas"
) as HTMLCanvasElement;
const symbolSeqForm = document.querySelector(
  ".symbol_sequence__form"
) as HTMLFormElement;
let resultTableBody = resultTable.querySelector(
  "tbody"
) as HTMLTableSectionElement;
const clearButton = document.getElementById("clear") as HTMLButtonElement;
const downloadButton = document.getElementById("export") as HTMLButtonElement;
const aboutButton = document.getElementById("about") as HTMLButtonElement;
const footnoteContainer = document.querySelector(
  ".footnote__container"
) as HTMLDivElement;

const charsMapInput: { [key: string]: number } = {};
let huffmanTree: HuffmanTree;

window.addEventListener("load", () => {
  addNewSymbolFormInputContainer();
});

aboutButton.addEventListener("click", () => {
  footnoteContainer.style.display = "block";
});

footnoteContainer.querySelector("button")?.addEventListener("click", () => {
  footnoteContainer.style.display = "none";
});

addToSymbolProbFormButton.addEventListener("click", () => {
  addNewSymbolFormInputContainer();
});

const addNewSymbolFormInputContainer = () => {
  console.log("Adding input container.");
  const newSymbolInputFormInputContainer =
    symbolProbInputContainerTemplate?.content?.cloneNode(true);

  (newSymbolInputFormInputContainer as Element)
    .querySelector(".delete__button")
    ?.addEventListener("click", (e) => {
      (e.target as Element).closest(".symbol_prob_container")?.remove();
    });
  symbolProbFormContainer.appendChild(newSymbolInputFormInputContainer);
};

symbolProbForm.addEventListener("submit", (e) => {
  e.preventDefault();
  let totalProbability = 0;
  Array.from(symbolProbFormContainer.children).map((element) => {
    const symbol = (element.children[0] as HTMLInputElement).value;
    const probability = (element.children[1] as HTMLInputElement).value;
    charsMapInput[symbol] = parseFloat(probability);
    totalProbability += parseFloat(probability);
  });
  if (Math.abs(totalProbability - 1) > 2e-2) {
    alert("Sum of symbol probabilities should = 1 for consistent results.");
  }

  visualizeHuffmanTree(charsMapInput);
});

symbolSeqForm.addEventListener("submit", (e) => {
  e.preventDefault();
  const symbolSeq = (document.getElementById("symbol_seq") as HTMLInputElement)
    .value;
  const charsMapInput = charProbabilityMap(symbolSeq);
  visualizeHuffmanTree(charsMapInput);
});

const visualizeHuffmanTree = (charsMapInput: { [key: string]: number }) => {
  // Clear previous visualization
  if (huffmanTree) {
    console.log("Clearing huffman tree.");
    clearHuffmanViz();
  }
  // Initialize the huffman tree
  huffmanTree = new HuffmanTree(
    charsMapInput,
    huffmanTreeCanvas.getContext("2d") as CanvasRenderingContext2D
  );
  // Generate the huffman codes
  const huffmanCodes = huffmanTree.gethuffmanCodes();
  // Visualize the average code word length, and entropy
  (
    (resultStatsContainer as Element).querySelector(
      ".average_codeword p"
    ) as HTMLElement
  ).innerText = `${huffmanTree.computeAverageCodewordLength()} bits`;
  (
    (resultStatsContainer as Element).querySelector(".entropy p") as HTMLElement
  ).innerText = `${+huffmanTree.computeEntropy().toFixed(5)} bits`;

  // Visualize the codes in a table
  Array.from(huffmanCodes).map((item, i) => {
    let row = resultTableBody.insertRow(i);
    let sn = row.insertCell(0);
    let symbol = row.insertCell(1);
    let proba = row.insertCell(2);
    sn.innerHTML = i.toString();
    symbol.innerHTML = item[0];
    proba.innerHTML = item[1].toString();
    resultContainer.style.display = "block";
  });

  // draw the huffman tree on the canvas
  huffmanTree.drawHuffmanTree();
};

const clearHuffmanViz = () => {
  resultTableBody.innerHTML = "";
  huffmanTree.ctx.clearRect(
    0,
    0,
    huffmanTreeCanvas.width,
    huffmanTreeCanvas.height
  );
  resultContainer.style.display = "none";
};

clearButton.addEventListener("click", () => {
  clearHuffmanViz();
});

downloadButton.addEventListener("click", () => {
  const filename = `huffmantree-${new Date().getTime()}.png`;
  huffmanTree.download(filename);
});

class HuffmanTreeNode {
  char: string;
  frequency: number;
  left: HuffmanTreeNode | null;
  right: HuffmanTreeNode | null;
  code: string;
  x: number | null;
  y: number | null;
  depth: number;
  ctx: CanvasRenderingContext2D;

  constructor(char: string, frequency: number, ctx: CanvasRenderingContext2D) {
    this.char = char;
    this.frequency = frequency;
    this.left = null;
    this.right = null;
    this.code = "";
    this.x = null;
    this.y = null;
    this.depth = 0;
    this.ctx = ctx;
  }

  get radius() {
    return this.char === "" ? 12 : 24;
  }

  // computes the left co-ordinate for the current node
  get leftCoordinate() {
    const offsetX = Math.round(Math.exp(5 - this.depth));
    return {
      cx: Number(this.x) - 3 * this.radius - offsetX,
      cy: Number(this.y) + 72,
    };
  }

  // computes the right co-ordinate for the current node
  get rightCoordinate() {
    const offsetX = Math.round(Math.exp(5 - this.depth));
    return {
      cx: Number(this.x) + 3 * this.radius + offsetX,
      cy: Number(this.y) + 72,
    };
  }

  drawEdgePath = (toX: number, toY: number, direction: "r" | "l") => {
    console.log(toX, toY, direction);
    const moveToX =
      direction === "r"
        ? Number(this.x) + this.radius
        : Number(this.x) - this.radius;
    const moveToY = Number(this.y) + this.radius - 10;
    const lineToX = direction === "r" ? toX : toX;
    const lineToY = toY;
    this.ctx.beginPath();
    // Begin a new sub-path for drawing the edge
    this.ctx.moveTo(moveToX, moveToY);
    // Move from (moveToX,moveToY) to (lineToX,lineToY)
    this.ctx.lineTo(lineToX, lineToY);
    this.ctx.lineWidth = 1.5;
    this.ctx.strokeStyle = "#000000";
    this.ctx.stroke();
    this.ctx.closePath();
  };

  draw() {
    this.ctx.beginPath();
    this.ctx.arc(Number(this.x), Number(this.y), this.radius, 0, 2 * Math.PI);
    this.ctx.fillStyle = "dodgerblue";
    this.ctx.fill();

    this.ctx.font = "16pt Calibri";
    this.ctx.fillStyle = "white";
    this.ctx.textAlign = "center";
    this.ctx.fillText(this.char, Number(this.x), Number(this.y) + 5);
  }
}

const charProbabilityMap = (input: string) => {
  let charMap: { [key: string]: number } = {};
  for (let i = 0; i < input.length; i++) {
    if (charMap[input[i]]) {
      charMap[input[i]] += 1;
    } else {
      charMap[input[i]] = 1;
    }
  }

  // compute probability for each key
  Object.keys(charMap).map((key) => {
    charMap[key] /= input.length;
  });
  return charMap;
};

class HuffmanTree {
  charMap: { [key: string]: number };
  codesMap: Map<string, string>;
  ctx: CanvasRenderingContext2D;
  rootNode: HuffmanTreeNode | null;

  constructor(input: { [key: string]: number }, ctx: CanvasRenderingContext2D) {
    // input is a object mapping each symbol to probability of occurence
    this.charMap = input;
    this.codesMap = new Map();
    this.ctx = ctx;
    this.rootNode = null;
  }

  // Build the huffman tree, returns the root node
  buildHuffmanTree() {
    let forest = [];
    // create a forest consisting of all the huffman tree nodes
    for (let char in this.charMap) {
      const treeNode = new HuffmanTreeNode(char, this.charMap[char], this.ctx);
      forest.push(treeNode);
    }
    while (forest.length > 1) {
      // sort the forest
      forest.sort((a, b) => {
        return a.frequency - b.frequency;
      });

      // merge the two smallest tree nodes
      let newNode = new HuffmanTreeNode(
        "",
        forest[0].frequency + forest[1].frequency,
        this.ctx
      );
      newNode.left = forest[0];
      newNode.right = forest[1];
      // delete the smallest tree nodes
      forest = forest.slice(2);

      // push the new node to the forest
      forest.push(newNode);
    }
    let rootNode = forest[0];
    rootNode.x = 400;
    rootNode.y = 30;
    return rootNode;
  }

  // Traverse the huffman tree to obtain the binary codes
  traverseHuffmanTree(node: HuffmanTreeNode) {
    if (node.left !== null) {
      // left node's code is 0 + code of parent node
      node.left.code = node.code + "0";
      // new node depth = parent node depth + 1
      node.left.depth = node.depth + 1;
      // compute the left node's co-ordinate
      const { cx, cy } = node.leftCoordinate;
      node.left.x = cx;
      node.left.y = cy;
      // recursively traverse the tree
      this.traverseHuffmanTree(node.left);
    }
    if (node.right !== null) {
      // right node's code is 1 + code of parent node
      node.right.code = node.code + "1";
      // new node depth = parent node depth + 1
      node.right.depth = node.depth + 1;
      // compute the right node's co-ordinate
      const { cx, cy } = node.rightCoordinate;
      node.right.x = cx;
      node.right.y = cy;
      // recursively traverse the tree
      this.traverseHuffmanTree(node.right);
    }
    // set the symbol's code in the codes map
    if (node.char in this.charMap) {
      this.codesMap.set(node.char, node.code);
    }
  }

  // Compute average code word length of the huffman encoding
  computeAverageCodewordLength() {
    let avgCodewordLength = 0;
    Object.keys(this.charMap).map((key) => {
      const prob = this.charMap[key];
      const codeLength = this.codesMap.get(key)?.length;
      avgCodewordLength += prob * Number(codeLength);
    });
    return avgCodewordLength;
  }

  // Compute entropy of the huffman encoding
  computeEntropy() {
    let entropy = 0;
    Object.keys(this.charMap).map((key) => {
      const prob = this.charMap[key];
      entropy += prob * Math.log2(1 / prob);
    });
    return entropy;
  }

  // returns the codes
  gethuffmanCodes() {
    const rootNode = this.buildHuffmanTree();
    this.traverseHuffmanTree(rootNode);
    this.rootNode = rootNode;
    return this.codesMap;
  }

  // draw tree edge code
  drawTreeEdgeCode(value: string, x: number, y: number) {
    this.ctx.font = "16pt Calibri";
    this.ctx.fillStyle = "#000000";
    this.ctx.textAlign = "center";
    this.ctx.fillText(value, x, y);
  }

  // draw the huffman tree
  drawHuffmanTree() {
    this.rootNode?.draw();
    const recursiveDrawNodes = (currentNode: HuffmanTreeNode) => {
      if (currentNode.left) {
        currentNode.left.draw();
        const { cx, cy } = currentNode.leftCoordinate;
        currentNode.drawEdgePath(cx, cy - currentNode.left.radius, "l");
        this.drawTreeEdgeCode(
          "0",
          cx + 10 * (5 - currentNode.left.depth),
          cy - 60
        );
        recursiveDrawNodes(currentNode.left);
      }
      if (currentNode.right) {
        currentNode.right.draw();
        const { cx, cy } = currentNode.rightCoordinate;
        currentNode.drawEdgePath(cx, cy - currentNode.right.radius, "r");
        this.drawTreeEdgeCode(
          "1",
          cx - 10 * (5 - currentNode.right.depth),
          cy - 60
        );
        recursiveDrawNodes(currentNode.right);
      }
    };
    recursiveDrawNodes(this.rootNode as HuffmanTreeNode);
  }

  download(filename: string) {
    /// create an "off-screen" anchor tag
    var lnk = document.createElement("a");
    /// the key here is to set the download attribute of the a tag
    lnk.download = filename;
    /// convert canvas content to data-uri for link. When download
    /// attribute is set the content pointed to by link will be
    /// pushed as "download" in HTML5 capable browsers
    lnk.href = this.ctx.canvas.toDataURL("image/png;base64");
    lnk.click();
  }
}

// const node = new HuffmanTreeNode()
// const seq = "ABBCCCDDDDEEEEE";
// const tree = new HuffmanTree({'s0':0.4,'s1':0.2,'s2':0.2,'s3':0.1,'s4':0.1})
// console.log(tree.charMap)
// const rootNode = tree.buildHuffmanTree()
// tree.traverseHuffmanTree(rootNode)
// console.log(tree.codesMap)
// console.log(tree.computeEntropy())
// console.log(tree.computeAverageCodewordLength())
