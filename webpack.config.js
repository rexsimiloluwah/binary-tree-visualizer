const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

const htmlPagesNames = ["index","huffman"]
const htmlPagesWebpackPlugins = htmlPagesNames.map(
  (name) =>
    new HtmlWebpackPlugin({
      template: `./src/${name}.html`, // relative path to the HTML file
      filename: `${name}.html`, // output HTML file
      chunks: [`${name}`]
    })
);
module.exports = {
  // bundling mode
  mode: "production",

  // entry files
  entry: {
    main: "./src/main.ts",
    huffman: "./src/huffman.ts"
  },

  // output bundles (location)
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "[name].js",
  },

  // file resolutions
  resolve: {
    extensions: [".ts", ".js",".tsx"],
  },

  // loaders
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: [
          { loader: "style-loader" },
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              publicPath: "css",
              esModule: false,
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        path.resolve(__dirname, "src", "style.css"),
        path.resolve(
          __dirname,
          "src",
          "huffman.css",
        ),
      ],
    }),
  ].concat(htmlPagesWebpackPlugins),
};
